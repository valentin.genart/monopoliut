package test;

import static org.junit.Assert.*;
import org.junit.Test;

import main.CaseTaxe;

public class TestCaseTaxe {
	
	private CaseTaxe caseTaxe = new CaseTaxe("Taxe de l'IUT", 100);
	
	@Test
	public void test() {
		assertEquals(caseTaxe.getNom(),"Taxe de l'IUT");
		assertFalse(caseTaxe.getPrix() == 50);
	}

}
