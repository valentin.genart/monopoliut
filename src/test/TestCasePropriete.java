package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import org.junit.Test;

import main.CasePropriete;
import main.Couleur;

public class TestCasePropriete {
	private CasePropriete[] cases = new CasePropriete[] {
												new CasePropriete("Salle 4A14", Couleur.Bleu, 20, new int[] {4,25,75,150,300,500}),
												new CasePropriete("Salle 4A16", Couleur.Bleu, 25, new int[] {5,28,85,160,320,550}),
												new CasePropriete("Salle 4A20", Couleur.Bleu, 15, new int[] {6,30,90,170,350,560}),
												new CasePropriete("Salle 0A20",Couleur.Rose, 15 , new int[] {5,35,90,160,380,440}),
												new CasePropriete("Salle 0A18",Couleur.Rose, 20 , new int[] {7,40,95,170,370,460}),
												new CasePropriete("Salle 0AA06",Couleur.Rose, 10 , new int[] {3,30,75,120,320,380}),
												new CasePropriete("Local BDE", Couleur.Jaune, 40, new int[] {6,30,100,170,350,600}),
												new CasePropriete("Secrétariat", Couleur.Jaune, 60, new int[] {10,50,120,200,380,650}),
												new CasePropriete("Le ponton", Couleur.Jaune, 70, new int[] {15,65,150,220,400,700}),
												new CasePropriete("R.U. Sully", Couleur.Marron, 60, new int[] {10,50,120,200,410,700}),
												new CasePropriete("R.U. Pariselle", Couleur.Marron, 65, new int[] {20,45,150,240,500,750}),
												new CasePropriete("R.U. Barois ", Couleur.Marron, 70, new int[] {15,40,130,220,470,715}),
												new CasePropriete("Mcdo", Couleur.Rouge, 60, new int[] {15,45,178,275,380,560}),
												new CasePropriete("Burker King", Couleur.Rouge, 70, new int[] {25,55,195,320,395,590}),
												new CasePropriete("Escalier", Couleur.Vert, 10, new int[] {2,15,90,180,250,300}),
												new CasePropriete("Ascenseur", Couleur.Vert, 20, new int[] {4,30,120,200,290,350}),
												new CasePropriete("Couloir", Couleur.Vert, 5, new int[] {1,10,60,100,150,300}),
												new CasePropriete("Parking", Couleur.Violet, 50, new int[] {18,50,150,250,300,450}),
												new CasePropriete("Reeflex", Couleur.Violet, 70, new int[] {25,75,170,290,350,550}),
												new CasePropriete("Amphi 1A06", Couleur.Orange, 80, new int[] {12,60,150,270,500,800}),												
												new CasePropriete("Amphi 1A14", Couleur.Orange, 85, new int[] {15,65,170,280,525,845}),
												new CasePropriete("Amphi 1A04", Couleur.Orange, 75, new int[] {10,50,135,280,475,750})											
};

														

	@Test
	public void test() {
		for(int i=0;i<cases.length;i++) {
			System.out.println(cases[i]+"\n");
		}
		assertTrue(cases[0].getNom().equals("Salle 4A14"));
		assertEquals(cases[1].getPrix(),25);
		assertFalse(cases[8].getCouleur().equals(Couleur.Rose));
	}

}
