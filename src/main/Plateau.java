package main;

public class Plateau {
	
	public final static Case [] plateau= new Case[40];
	
	public static void create() {
		plateau[0] = new CaseDepart();
		plateau[1] = new CasePropriete("Salle 4A14", Couleur.Bleu, 20, new int[] {4,25,75,150,300,500});
		plateau[2] = new CaseQuestion("Q1");
		plateau[3] = new CasePropriete("Salle 4A16", Couleur.Bleu, 25, new int[] {5,28,85,160,320,550});
		plateau[4] = new CasePropriete("Salle 4A20", Couleur.Bleu, 15, new int[] {6,30,90,170,350,560});
		plateau[5] = new CaseGare("G1",100,false);
		plateau[6] = new CasePropriete("Salle 0A20",Couleur.Rose, 15 , new int[] {5,35,90,160,380,440});
		plateau[7] = new CasePropriete("Salle 0A18",Couleur.Rose, 20 , new int[] {7,40,95,170,370,460});
		plateau[8] = new CaseQuestion("Q2");
		plateau[9] = new CasePropriete("Salle 0AA06",Couleur.Rose, 10 , new int[] {3,30,75,120,320,380});
		plateau[10] = new CasePrison("Prison");
		plateau[11] = new CasePropriete("Local BDE", Couleur.Jaune, 40, new int[] {6,30,100,170,350,600});
		plateau[12] = new CaseTaxe("Electricité",100);
		plateau[13] = new CasePropriete("Secrétariat", Couleur.Jaune, 60, new int[] {10,50,120,200,380,650});
		plateau[14] = new CasePropriete("Le ponton", Couleur.Jaune, 70, new int[] {15,65,150,220,400,700});
		plateau[15] = new CaseGare("G2", 100, false);
		plateau[16] = new CasePropriete("R.U. Sully", Couleur.Marron, 60, new int[] {10,50,120,200,410,700});
		plateau[17] = new CaseQuestion("Q3");
		plateau[18] = new CasePropriete("R.U. Pariselle", Couleur.Marron, 65, new int[] {20,45,150,240,500,750});
		plateau[19] = new CasePropriete("R.U. Barois ", Couleur.Marron, 70, new int[] {15,40,130,220,470,715});
		plateau[20] = new CaseParc("Parc");
		plateau[21] = new CasePropriete("Mcdo", Couleur.Rouge, 60, new int[] {15,45,178,275,380,560});
		plateau[22] = new CaseQuestion("Q4");
		plateau[23] = new CasePropriete("Burker King", Couleur.Rouge, 70, new int[] {25,55,195,320,395,590});
		plateau[24] = new CaseGare("G3",100,false);
		plateau[25] = new CasePropriete("Escalier", Couleur.Vert, 10, new int[] {2,15,90,180,250,300});
		plateau[26] = new CasePropriete("Ascenseur", Couleur.Vert, 20, new int[] {4,30,120,200,290,350});
		plateau[27] = new CaseTaxe("Eau",100);
		plateau[28] = new CasePropriete("Couloir", Couleur.Vert, 5, new int[] {1,10,60,100,150,300});
		plateau[29] = new CaseQuestion("Q5");
		plateau[30] = new CasePolice("Police");
		plateau[31] = new CasePropriete("Parking", Couleur.Violet, 50, new int[] {18,50,150,250,300,450});
		plateau[32] = new CasePropriete("Reeflex", Couleur.Violet, 70, new int[] {25,75,170,290,350,550});
		plateau[33] = new CaseTaxe("Imp�t",200);
		plateau[34] = new CaseGare("G4",100,false);
		plateau[35] = new CasePropriete("Amphi 1A04", Couleur.Orange, 75, new int[] {10,50,135,280,475,750});
		plateau[36] = new CaseQuestion("Q6");
		plateau[37] = new CasePropriete("Amphi 1A06", Couleur.Orange, 80, new int[] {12,60,150,270,500,800});
		plateau[38] = new CasePropriete("Amphi 1A14", Couleur.Orange, 85, new int[] {15,65,170,280,525,845});
		plateau[39] = new CaseQuestion("Q7");
	}
	
	public static String affiche() {
		String res = "";
		res+=	 "+---------+---------+---------+---------+---------+---------+---------+---------+---------+---------+---------+\n"
			    +"|20       |21       |22       |23       |24       |25       |26       |27       |28       |29       |30       |\n"
				+"|  PARC   |   MCDO  |QUESTION |BURGER K |  GARE 3 |ESCALIER |ASCENCEUR|  TAXE   | COULOIR |QUESTION | POLICE  |\n"
				+"|         |         |         |         |         |         |         |         |         |         |         |\n"
				+"|---------+---------+---------+---------+---------+---------+---------+---------+---------+---------+---------|\n"
				+"|19       |                                                                                         |31       |\n"
				+"|  BAROIS |                                                                                         | PARKING |\n"
				+"|         |                                                                                         |         |\n"
				+"|---------|                                                                                         |---------|\n"
				+"|18       |                                                                                         |32       |\n"
				+"|PARISELLE|                                                                                         | REEFLEX |\n"
				+"|         |                                                                                         |         |\n"
				+"|---------|                                                                                         |---------|\n"
				+"|17       |                                                                                         |33       |\n"
				+"|QUESTION |                                                                                         |  TAXE   |\n"
				+"|         |                                                                                         |         |\n"
				+"|---------|                                                                                         |---------|\n"
				+"|16       |                                                                                         |34       |\n"
				+"|  SULLY  |                                                                                         |  GARE 4 |\n"
				+"|         |                                                                                         |         |\n"
				+"|---------|                                                                                         |---------|\n"
				+"|15       |                                                                                         |35       |\n"
				+"| GARE 2  |                                                                                         |   1A04  |\n"
				+"|         |                                                                                         |         |\n"
				+"|---------| 																						  |---------|\n"
				+"|14       |                                                                                         |36       |\n"
				+"| PONTON  |                                                                                         |QUESTION |\n"
				+"|         |                                                                                         |         |\n"
				+"|---------|                                                                                         |---------|\n"
				+"|13       |                                                                                         |37       |\n"
				+"| BUREAU  |                                                                                         |   1A06  |\n"
				+"|         |                                                                                         |         |\n"
				+"|---------|                                                                                         |---------|\n"
				+"|12       |                                                                                         |38       |\n"
				+"|  TAXE   |                                                                                         |   1A14  |\n"
				+"|         |                                                                                         |         |\n"
				+"|---------|                                                                                         |---------|\n"
				+"|11       |                                                                                         |39       |\n"
				+"|LOCAL BDE|                                                                                         |QUESTION |\n"
				+"|         |                                                                                         |         |\n"
				+"|---------+---------+---------+---------+---------+---------+---------+---------+---------+---------+---------|\n"
				+"|10       |9        |8        |7        |6        |5        |4        |3        |2        |1        |0        |\n"
				+"| PRISON  |  0AA06  | QUESTION|  0A18   |  0A20   | GARE 1  |  4A20   |  4A16   |QUESTION |   4A14  |  DEPART |\n"
				+"|         |         |         |         |         |         |         |         |         |         |         |\n"
				+"+---------+---------+---------+---------+---------+---------+---------+---------+---------+---------+---------+\n";
		return res;
	}

	public static void main(String[]args) {
		Plateau.create();
		System.out.println(Plateau.affiche());
	}
}