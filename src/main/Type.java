package main;

public enum Type {
	CasePropriete("PROPRIETE"),
	CaseGare("GARE"),
	CaseQuestions("QUESTIONS"),
	CaseTaxe("Taxe"),
	CaseDepart("DEPART"),
	CasePrison("PRISON"),
	CaseParc("PARC"),
	CasePolice("POLICE");
	
	private String type;
	
	Type (String type){
		this.type = type;
	}
	
	public String toString() {
		return type;
	}
}
