package main;

/**
 * Classe abstraite représentant une case.
 * @author genartv
 *
 */
public abstract class Case {
	
	/**
	 * Le nom de la case.
	 */
	protected String nom;
	/**
	 * Le type de la case.
	 */
	protected Type type;
	String joueursdessus;

	public Case(String nom, Type type) {
		super();
		this.nom = nom;
		this.type = type;
		this.joueursdessus="";
	}
	
	/**
	 * Retourne le nom de la case.
	 * @return
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Retourne le Type de la case.
	 * @return
	 */
	public Type getType() {
		return type;
	}

	@Override
	public String toString() {
		return "Case [nom=" + nom + ", type=" + type + "]";
	}
	
	/**
	 * Méthode définissant les évènements qui se produisent lorsque l'on tombe sur la case, en fonction du type de la case.
	 * @param j
	 */
	public abstract void play(Joueur j);
	
	public void poserJoueur(String nom) {
		joueursdessus+=nom+" ";
	}
}
