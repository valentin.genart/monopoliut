package main;

import java.util.Random;
/**
 * 
 * @author Sami Lejeune
 *
 */
public class Tirage {
	
	private Random rnd = new Random();
	/**
	 * Tirage minimum = 1
	 */
	private final int MIN_TIRAGE=1;
	/**
	 * Tirage maximum = 6; 
	 */
	private final int MAX_TIRAGE = 6;
	private boolean estDouble;
	private int cpt_double;
	private int de1,de2,resultat;

	public Tirage() {
		estDouble=false;
		cpt_double=0; 
		de1=0;
		de2=0;
	}
	/** Méthode retournant la valeur du tirage des 2 dés.  
	 * 
	 * @return la valeur du tirage 
	 */
	public int tirage() {
		de1 = tirageNombre(MIN_TIRAGE, MAX_TIRAGE);
		de2 = tirageNombre(MIN_TIRAGE, MAX_TIRAGE);
		calculEstDouble(de1,de2);
		resultat = de1+de2;
		return resultat; 
	}
	
	
	private void calculEstDouble(int x , int y) {
		if (x==y) {
			estDouble = true;
			cpt_double++;
		}else {
			estDouble=false;
			cpt_double=0;
		}
		
	}
	/**
	 * 
	 * @return Si le tirage a sorti un double
	 */
	public boolean estDouble() {
		return this.estDouble;
	}
	private int tirageNombre(int min, int max) {
		return rnd.nextInt(max - min + 1) + min;
	}
	public int getCpt_double() {
		return cpt_double;
	}
	public void restart() {
		cpt_double=0;
	}
	public int getDe1() {
		return de1;
	}
	public int getDe2() {
		return de2;
	}
	public int getResultat() {
		return resultat;
	}
	
}
