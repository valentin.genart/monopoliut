package main;

public enum Couleur {
	Marron("MARRON"), //LES R.U. : 3
	Bleu("BLEU"), //Salle TP : 3 
	Rose("ROSE"), // SALLE TD : 3
	Orange("ORANGE"), //AMPHI : 3
	Rouge("ROUGE"), //FAST-FOOD : 2 
	Jaune("JAUNE"), //LIEUX IMPORTANTS : 3
	Vert("VERT"), //ASCENSEUR,ESCALIER.. : 3
	Violet("VIOLET"); //INFRASTRUCTURE EXTERIEURES : 2
	
	private String couleur;
	
	Couleur(String couleur){
		this.couleur = couleur;
	}
	
	public String toString() {
		return couleur;
	}
}
