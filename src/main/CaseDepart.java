package main;
/**
 * Classe repr�sentant la case D�part.
 * @author genartv
 *
 */
public class CaseDepart extends Case{

	public CaseDepart() {
		super("DEPART", Type.CaseDepart);
	}

	@Override
	public void play(Joueur j) {
		if(j.estHumain()) {
			System.out.println("Vous êtes sur la case départ");		
		} else {
			System.out.println("L'IA est sur la case départ");
		}
		
	}

}
