package main;
/**
 * Représente une intelligence artificielle.
 * @author genartv
 *
 */
public class Ordi extends Joueur{

	public Ordi(String nom) {
		super(nom);
	}
	
	public boolean estHumain() {
		return false;
	}
}
