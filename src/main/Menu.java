package main;

import java.util.Scanner;
/**
 * Classe affichant un menu pour choisir si on veut jouer contre une IA, ou contre d'autres joueurs. 
 * @author genartv
 *
 */
public class Menu {

	public static void affiche() {
		Scanner sc = new Scanner(System.in);
		String choix;

		System.out.println();
		System.out.println("********* MONOPOL'IUT *********");
		System.out.println("*                             *");
		System.out.println("*    Bienvenue sur le jeu     *");
		System.out.println("*                             *");
		System.out.println("*******************************");
		System.out.println("*                             *");
		System.out.println("*  1 : Joueur vs IA           *");
		System.out.println("*                             *");
		System.out.println("*  2 : Joueur vs Joueur       *");
		System.out.println("*                             *");
		System.out.println("*******************************");
		boolean choixOk;
		System.out.println();
		do {
			choixOk=false;
			System.out.print("Veuillez indiquer votre choix : ");
			do {
				choix = sc.nextLine();
				if (!(choix.length() >0 )) System.out.println("Tu n'as pas fait de choix jeune padawan ;)");
			} while(!(choix.length() >0 ));
			if (choix.charAt(0) == '1') {
				Moteur.jouer(true);
				choixOk = true;
			} else if(choix.charAt(0) == '2') {
				Moteur.jouer(false);
				choixOk = true;
			}
		} while(!choixOk);

	}

	public static void main(String[] args) {
		Menu.affiche();
	}
}
