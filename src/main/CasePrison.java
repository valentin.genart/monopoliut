package main;
/**
 * Classe représentant la Prison.
 * @author genartv
 *	@author lejeunes
 */
public class CasePrison extends Case{
	
	public CasePrison(String nom) {
		super(nom, Type.CasePrison);
	}


	@Override
	public void play(Joueur joueur) {
		if (!joueur.estHumain())
			System.out.println("L'IA rend visite aux serveurs de la prison.");
		else
			System.out.println("Vous êtes sur la case prison (simple visite)!");
	}
}
