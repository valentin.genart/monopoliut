package main;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.Random;
import java.util.Scanner;

/**
 * 
 * @author Theo Canonne
 *	@author lejeunes
 */

public class CaseQuestion extends Case {
	
	BufferedReader br;
	String emplacementCSV = "ressources/questions.csv";
	
	private String[][] questions;
	
	public CaseQuestion(String nom) {
		super(nom, Type.CaseQuestions);
		
		try {
			br = new BufferedReader(new FileReader(emplacementCSV));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		
		String line = "";
		int cpt = 0;
		
		// compte nbr lignes pour les tableaux questions
		try {
			while ((line = br.readLine()) != null) {

			    cpt ++;

			}
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		questions = new String[cpt][6];
		
		//---------------------------------------------
		
		// enregistre les question
		cpt = 0;
		try {
			br = new BufferedReader(new FileReader(emplacementCSV));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
        try {
			while ((line = br.readLine()) != null) {

			    questions[cpt++] = line.split(";");
			    
			}
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
	
	
	
	/**
	 * Propose au joueur de répondre à une question aléatoire.
	 */
	@Override
	public void play(Joueur j) {
		if (j.estHumain()) {
			System.out.println("Vous êtes sur une case question, répondez-bien sinon vous perdez 100€ !");
			Random r = new Random();
			int question = r.nextInt(12);
			
			System.out.println("Question : " + questions[question][0] 
					+ "\n Reponse 1 : " + questions[question][1]
					+ "\n Reponse 2 : " + questions[question][2]
					+ "\n Reponse 3 : " + questions[question][3]
					+ "\n Reponse 4 : " + questions[question][4]
					+ "\n Quel est votre réponse ? (a,b,c,d)");
			
			Scanner sc = new Scanner(System.in);
			
			String reponse = "";
			
			do {
				reponse = sc.nextLine();
				if (!(reponse.equalsIgnoreCase("a")||reponse.equalsIgnoreCase("b")||reponse.equalsIgnoreCase("c")||reponse.equalsIgnoreCase("d"))) System.out.println("Tu n'as pas entré une bonne valeur !");
			}while (!(reponse.equalsIgnoreCase("a")||reponse.equalsIgnoreCase("b")||reponse.equalsIgnoreCase("c")||reponse.equalsIgnoreCase("d")));
			
			if (reponse.equals(questions[question][5])) {
				j.setCaisse(j.getCaisse()+100);
				System.out.println("Bien joué (+100€) !"); 
			}
			else {
				System.out.println("Dommage la bonne réponse était la réponse : " + questions[question][5] + " (-100€)");
				j.setCaisse(j.getCaisse()-100);
			}
		} else {
			System.out.println("L'IA est une machine et connaît tout , elle récupère 100€");
			j.setCaisse(j.getCaisse()+100);
		}
	}	
}
