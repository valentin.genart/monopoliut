package main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

/**
 * Classe représentant les cases Gare.
 * @author genartv
 *	@author lejeunes
 */
public class CaseGare extends Case{
	protected int prix;
	protected boolean possede;
	private Joueur courant;
	public CaseGare(String nom, int prix, boolean possede) {
		super(nom, Type.CaseGare);
		this.prix = prix;
		this.possede = possede;
		courant = null;
	}
	public int getPrix() {
		return prix;
	}
	public boolean isPossede() {
		return possede;
	}
	public void setPossede(boolean possede) {
		this.possede = possede;
	}
	
	public String toString() {
		return "Nom: "+super.getNom()+"\nType: "+super.getType()+"\nPrix: " + prix ;
	}
	
	public void play(Joueur j) {
		if (j.estHumain()) {
			System.out.println("Vous êtes sur la case d'une gare : \n" + this + "\n" );
			if (!possede) {
				
				boolean reponseCorrect = false;
				while(!reponseCorrect) {
					System.out.println("La case n'appartient à personne , voulez-vous l'acheter. Prix = " + this.prix + "€ (O/N)");
					Scanner sc = new Scanner(System.in);
					String reponse = sc.nextLine();
					if (reponse.equalsIgnoreCase("O")) {
						if (j.getCaisse() - this.prix >=0) {
							j.ajoutCase(this);
							j.setCaisse(j.getCaisse()-prix);
							possede = true;
							courant = j;
						} else {
							System.out.println("Vous n'avez pas assez d'argent pour acheter la propriété.");
						}
						reponseCorrect = true;
					} else if (reponse.equalsIgnoreCase("N")) {
						reponseCorrect = true;
					}
				}
				
			} else {
				if (!courant.equals(j)) {
					ArrayList<Case> casesJoueurs = j.getMesPossessions();
					int quantiteGare=0;
					for (Case c : casesJoueurs) {
						if (c.getType() == Type.CaseGare) {
							quantiteGare++;
						}
					}
					
					System.out.println("La case ne vous appartient pas , vous payez la somme de " +(quantiteGare* this.prix) + "€ pour le joueur " + courant.getNom());
					courant.setCaisse(courant.getCaisse()+prix);
				} else {
					System.out.println("Vous êtes sur votre propriété.");
				}
			}
		}  else {
			System.out.println("L'IA tombe sur la case : \n " + this);
			if (!possede) {
				Random r = new Random();
				int decisionRandom = r.nextInt(3); // 0 ou 1 -> OUI 2-> NON
				if (decisionRandom < 2) {
					if (j.getCaisse() - this.prix >=0) {
						System.out.println("L'IA a bien acheté la propriété");
						j.ajoutCase(this);
						j.setCaisse(j.getCaisse()-prix);
						possede = true;
						courant = j;
					} else {
						System.out.println("L'IA n'a pas assez d'argent pour acheter la propriété.");
					}
				} else {
					System.out.println("L'IA n'a pas acheté..");
				}
			} else {
				if (!courant.equals(j)) {
					System.out.println("L'IA tombe sur votre propriété et vous donne " + prix + "€.");
					courant.setCaisse(courant.getCaisse()+prix);
				}else {
					System.out.println("L'IA est sur sa propriété.");
				}
			}
		}
		
		
	}

	
}