package main;

import java.util.Scanner;
/**
 * Classe gérant le déplacement d'un joueur.
 * @author genartv
 *
 */
public class Deplacement {
	public static void deplacement(Joueur joueur, Tirage tirage) {
		System.out.println(joueur.getNom()+": c'est votre tour.\n Appuyez sur Entrée");
		Scanner sc = new Scanner(System.in);
		sc.nextLine();
		int val=tirage.tirage();
		if(joueur.isPrisonier()) {
			joueur.setTour_prison(joueur.getTourPrison()+1);
			System.out.println("Vous êtes en prison depuis"+joueur.getTourPrison());
			if(tirage.estDouble()) joueur.setPrisonier(false);
			if(joueur.getTourPrison() == 3) joueur.setPrisonier(false);
		}

		if(tirage.getCpt_double() == 3) { 
			joueur.setPrisonier(true);
			joueur.setPlacement(10);
		}
		
		if(!joueur.isPrisonier()) {
			if(joueur.getPlacement() < 40 && joueur.getPlacement()+val > 40) {
				System.out.println("Vous êtes passé par la case DEPART vous recevez 200€");
				joueur.setCaisse(joueur.getCaisse()+200);
			}	
			joueur.setPlacement((joueur.getPlacement()+val)%40);
			System.out.println(Plateau.affiche());
			joueur.afficherInfos();
			System.out.println("Vous lancez les dés et obtenez: "+tirage.getDe1()+"+"+tirage.getDe2()+" = "+val);
			System.out.println("Vous êtes sur la case "+joueur.getPlacement());
			Plateau.plateau[joueur.getPlacement()].play(joueur);
		}
	}
}