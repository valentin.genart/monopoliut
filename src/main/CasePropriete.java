package main; 

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

import main.Case;
import main.Couleur;
import main.Type;

/**
 * Classe représentant une case Propriété, comprenant une couleur, un prix d'achat et des loyers à percevoir.
 * @author genartv
 *	@author lejeunes
 */
public class CasePropriete extends Case{
	
	protected Couleur couleur;
	protected int prix;
	protected int[] loyers;
	protected boolean hypotheque;
	protected boolean possede;
	private Joueur courant;
	private int nbMaison;
	
	public CasePropriete(String nom, Couleur couleur, int prix, int[] loyers) {
		super(nom, Type.CasePropriete);
		this.hypotheque = false;
		this.loyers = new int[6];
		this.couleur = couleur;
		this.prix = prix;
		this.possede = false;
		nbMaison = 0;
		for(int i=0; i<loyers.length; i++) {
			this.loyers[i] = loyers[i];
			courant = null;
		}
	}
	
	/**
	 * Retourne la couleur du groupe auquel appartient la carte.
	 * @return
	 */
	public Couleur getCouleur() {
		return couleur;
	}
	
	/**
	 * Retourne le prix d'achat de la carte.
	 * @return
	 */
	public int getPrix() {
		return prix;
	}
	/**
	 * Retourne la valeur de la carte lorsqu'elle est hypothéquée.
	 * @return
	 */
	public int getPrixHypotheque() {
		return prix/2;
	}

	/**
	 * Retourne true si la propriété est hypothéquée.
	 * @return
	 */
	public boolean isHypotheque() {
		return hypotheque;
	}

	/**
	 * Permet d'hypothéquer ou de déshypothéquer la propriété.
	 * @param hypotheque
	 */
	public void setHypotheque(boolean hypotheque) {
		this.hypotheque = hypotheque;
	}

	@Override
	public String toString() {
		String barrenom="\t\t\t|";
		String barrecouleur="\t\t\t|";
		if(super.getNom().length()<10)
			barrenom="\t\t\t\t|";
		if(couleur.toString().length()<6)
			barrecouleur="\t\t\t\t|";
		return "|Nom: "+super.getNom()+barrenom+"\n|Type: "+super.getType()+"\t\t\t|\n|Couleur: " + couleur + barrecouleur +"\n|Prix: " + prix + "\t\t\t\t|\n|Loyers: " + Arrays.toString(loyers) + "]\t|";
	}


	@Override
	public void play(Joueur j) {
		if (j.estHumain()) {
			System.out.println("Vous êtes tombé sur une case de propri�t� : \n" + this + "\n");
			if (!possede) {
				
				boolean reponseCorrect = false;
				while(!reponseCorrect) {
					System.out.println("La case n'appartient à personne, voulez-vous l'acheter. Prix = " + prix + "€ (O/N)");
					Scanner sc = new Scanner(System.in);
					String reponse = sc.nextLine();
					if (reponse.equalsIgnoreCase("O")) {
						if (j.getCaisse() - prix >=0) {
							j.ajoutCase(this);
							j.setCaisse(j.getCaisse()-prix);
							possede = true;
							courant = j;
						} else {
							System.out.println("Vous n'avez pas assez d'argent pour acheter la propriété.");
						}
						reponseCorrect = true;
					} else if (reponse.equalsIgnoreCase("N")) {
						reponseCorrect = true;
					}
				}
			} else {
				if (!courant.equals(j)) {
					System.out.println("La case ne vous appartient pas , vous payez la somme de " + loyers[nbMaison] + "€ pour le joueur " + courant.getNom());
					courant.setCaisse(courant.getCaisse()+loyers[nbMaison]);
				}else {
					System.out.println("Vous êtes sur votre propriété.");
				}
			}
		} else {
			System.out.println("L'IA tombe sur la case : \n " + this);
			if (!possede) {
				Random r = new Random();
				int decisionRandom = r.nextInt(3); // 0 ou 1-> OUI 2-> NON 
				if (decisionRandom < 2) {
					if (j.getCaisse() - prix >=0) {
						System.out.println("L'IA a bien acheté la propriété");
						j.ajoutCase(this);
						j.setCaisse(j.getCaisse()-prix);
						possede = true;
						courant = j;
					} else {
						System.out.println("L'IA n'a pas assez d'argent pour acheter la propriété.");
					}
				} else {
					System.out.println("L'IA n'a pas acheté...");
				}
			} else {
				if (!courant.equals(j)) {
					System.out.println("L'IA tombe sur votre propriété et vous donne " + loyers[nbMaison] + "€.");
					courant.setCaisse(courant.getCaisse()+loyers[nbMaison]);
				}else {
					System.out.println("L'IA est sur sa propriété.");
				}
				
			}
		}
		
		
	}
}
