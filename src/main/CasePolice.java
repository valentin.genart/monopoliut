package main;
/**
 * Classe représentant la Police, emmenant le joueur directement en prison.
 * @author genartv
 *
 */
public class CasePolice extends Case {

	public CasePolice(String nom) {
		super(nom, Type.CasePolice);
	}

	@Override
	public void play(Joueur joueur) {
		if(joueur.estHumain())
			System.out.println("Vous êtes tombé sur la case de la police! Direction prison...");
		else
			System.out.println("L'IA est tombée sur la case de la police et va en prison.");
			joueur.setPlacement(10);
		joueur.setPrisonier(true);
	}
}
