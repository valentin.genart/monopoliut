package main;
/**
 * Classe repr�sentant le Parc Gratuit, permettant de remporter l'argent au milieu du plateau.
 * @author genartv
 *	@author lejeunes
 */
public class CaseParc extends Case {
	
	protected static int argent_milieu;

	public CaseParc(String nom) {
		super(nom, Type.CaseParc);
		argent_milieu = 0;
	}

	/**
	 * Méthode permettant de récupérer l'argent au milieu.
	 * @return
	 */
	public int getArgentMilieu() {
		int res = argent_milieu;
		argent_milieu = 0;
		return res;
	}

	/**
	 * Méthode permettant d'ajouter une certaine somme à la cagnotte au milieu.
	 * @param somme
	 */
	public void addArgentMilieu(int somme) {
		this.argent_milieu += somme;
	}

	@Override
	public void play(Joueur joueur) {
		if (joueur.estHumain())
			System.out.println("Vous êtes tombé sur la case Parc, vous récupérez " + getArgentMilieu() + "€.");
		else
			System.out.println("L'IA est tombée sur la case Parc et récupère " + getArgentMilieu() + "€.");
		joueur.setCaisse(joueur.getCaisse()+argent_milieu);
		argent_milieu = 0;
	}
}
