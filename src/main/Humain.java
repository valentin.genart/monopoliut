package main;
/**
 * Représente un joueur humain.
 * @author genartv
 *
 */
public class Humain extends Joueur{

	public Humain(String nom) {
		super(nom);
	}

	public boolean estHumain() {
		return true;
	}
}
