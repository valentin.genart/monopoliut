package main;
/**
 * Représente un joueur, humain ou artificiel.
 */
import java.util.ArrayList;

public abstract class Joueur {
	private String nom;
	private int caisse;
	private int numero;
	private int placement;
	private static int pion=1;
	

	private ArrayList<Case> mesPossessions;

	private boolean prisonier;
	private int tour_prison=0;

	
	public Joueur(String nom) {
		this.nom = nom;
		this.caisse = 500;
		this.numero=pion;
		pion = pion+1;
		placement=0;
		this.mesPossessions = new ArrayList<Case>();
	}

	/**
	 * Retourne le nom du joueur.
	 * @return
	 */
	public String getNom() {
		return nom;
	}
	
	/**
	 * Définit le nom du joueur.
	 * @param nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * Retourne la somme d'argent possédée par le joueur.
	 * @return
	 */
	public int getCaisse() {
		return caisse;
	}
	
	/**
	 * Définit la somme d'argent possédée par le joueur.
	 * @param caisse
	 */
	public void setCaisse(int caisse) {
		this.caisse = caisse;
	}
	
	/**
	 * Retourne le numero du joueur.
	 * @return
	 */
	public int getNumero() {
		return numero;
	}
	
	/**
	 * Retourne true si le joueur est un humain, false sinon.
	 * @return
	 */
	public abstract boolean estHumain();

	public int getPlacement() {
		return placement;
	}
	
	/**
	 * Place le joueur sur le plateau. 
	 * @param placement
	 */
	public void setPlacement(int placement) {
		this.placement = placement;
	}
	
	/**
	 * Retourne true si le joueur est en prison.
	 * @return
	 */
	public boolean isPrisonier() {
		return prisonier;
	}
	
	/**
	 * Met le joueur en prison ou le sort de prison en fonction du booléen passé en paramÃ¨tre.
	 * @param prisonier
	 */
	public void setPrisonier(boolean prisonier) {
		this.prisonier = prisonier;
		this.tour_prison=0;
	}
	
	/**
	 * Retourne le nombre de tours depuis lequel le joueur est en prison.
	 * @return
	 */
	public int getTourPrison() {
		return tour_prison;
	}

	/**
	 * Définit le nombre de tours depuis lequel le joueur est en prison.
	 * @param tour_prison
	 */
	public void setTour_prison(int tour_prison) {
		this.tour_prison = tour_prison;
	}
	
	/**
	 * Ajoute une propriété à la liste de propriétés du joueur.
	 * @param c
	 */
	public void ajoutCase(Case c) {
		if (!this.mesPossessions.contains(c)) {
			this.mesPossessions.add(c);
		}
	}
	
	/**
	 * Retire une propriété à la liste de propriétés du joueur.
	 * @param c
	 */
	public void retireCase(Case c) {
		if (this.mesPossessions.contains(c)) {
			this.mesPossessions.remove(c);
		}
	}
	
	/**
	 * Affiche les propriétés possédées par le joueur.
	 */
	public void afficherPossession() {
		for (Case c : mesPossessions) {
			System.out.println("┌---------------------------------------┐");
			System.out.println(c);
			System.out.println("└---------------------------------------┘");
		}
	}
	
	/**
	 * Affiche toutes les informations du joueur.
	 */
	public void afficherInfos() {
		String barrenom="\t\t\t|";
		if(nom.length()<5)
			barrenom="\t\t\t\t|";
		System.out.println("┌-----------------------------------------------------------------------┐\n"+
					       "|                          "+nom+"      Joueur n° "+(numero-1)+barrenom+"\n"+
					       "|                          Argent: "+caisse+"\t\t\t\t\t|\n");
		afficherPossession();
		System.out.println("└-----------------------------------------------------------------------┘");
	}

	public ArrayList<Case> getMesPossessions() {
		return mesPossessions;
	}
	
	
}
