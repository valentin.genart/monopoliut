package main;

import java.util.ArrayList;
import java.util.Scanner;

public class Moteur {
	private static Scanner sc;

	public static void jouer(boolean ia) {
		Joueur gagnant = new Humain("joueur");
		if (!ia) {
			ArrayList<Joueur> liste = new ArrayList <Joueur>();
			Plateau.create();
			String nbjoueurs_string;
			int nbjoueurs=0;
			sc = new Scanner(System.in);
			do {
				System.out.println("Nombre de joueurs: (entre 2 et 4)");
				nbjoueurs_string = sc.nextLine();
				if (!(nbjoueurs_string.length() < 1)) {
					char nbjoueurs_char = nbjoueurs_string.charAt(0);
					nbjoueurs = nbjoueurs_char-48;
				} else {
					System.out.println("Oups tu n'as pas entré le nombre de Joueurs");
				}

			}while(nbjoueurs<2 || nbjoueurs>4 || nbjoueurs_string.length()!=1 || nbjoueurs_string.length() < 1);

			for(int i=0;i<nbjoueurs;i++) {
				String nomjoueur;
				do{
					System.out.println("Joueur "+i+", entrez votre prénom: ");
					nomjoueur = sc.nextLine();
					if (nomjoueur.length() < 1) System.out.println("Désolé ! Votre prénom est trop long, ou alors il est vide :(");
				} while (nomjoueur.length() < 1 || nomjoueur.length() > 12);
				liste.add(new Humain(nomjoueur)); 
			}

			int boucle=0; 
			Joueur j=liste.get(boucle);

			while(liste.size()>1) {

				Tirage t = new Tirage();

				do {
					Deplacement.deplacement(j, t);
					if(t.getDe1()==t.getDe2())
						System.out.println("Appuyer sur Entrée pour rejouer...");
					else
						System.out.println("Appuyer sur Entrée pour passer au joueur suivant...");
					sc.nextLine();
				}
				while(t.estDouble() && t.getCpt_double()<3 && !j.isPrisonier());
				if (j.getCaisse() <= 0) {
					liste.remove(j);
					System.out.println("Le joueur "+ j.getNom() + " n'est plus de la partie !\n");
				}
				boucle=(boucle+1)%liste.size();
				j=liste.get(boucle);		
			}
			gagnant = liste.get(0);
		} else {
			Plateau.create();
			ArrayList<Joueur> liste = new ArrayList <Joueur>();
			System.out.println("JEU AVEC IA");
			sc = new Scanner(System.in);
			String nomjoueur;
			do {
			System.out.println("Entrez votre prénom: ");
				nomjoueur = sc.nextLine();
				if (nomjoueur.length() < 1) System.out.println("Désolé ! Votre prénom est trop long, ou alors il est vide :(");
			} while (nomjoueur.length() < 1 || nomjoueur.length() > 12);
			Joueur reel = new Humain(nomjoueur);
			Joueur j_IA = new Ordi("SECQ"); 
			liste.add(reel);
			liste.add(j_IA);

			int boucle=0; 
			Joueur j=liste.get(boucle);
			Joueur perdant = null;
			while(liste.size()>1) {

				Tirage t = new Tirage();
				do {
					Deplacement.deplacement(j, t);
					if(t.getDe1()==t.getDe2())
						System.out.println("Appuyer sur Entrée pour rejouer...");
					else
						System.out.println("Appuyer sur Entrée pour passer au joueur suivant...");
					sc.nextLine();
				}
				while(t.estDouble() && t.getCpt_double()<3 && !j.isPrisonier());
				if (j.getCaisse() <= 0) {
					perdant = j;
					liste.remove(j);
					System.out.println("Le joueur "+ j.getNom() + " n'est plus de la partie !\n");
				}
				boucle=(boucle+1)%liste.size();
				j=liste.get(boucle);
			}
			gagnant = liste.get(0);

		}
		Fin.afficher(gagnant);
	}
} 