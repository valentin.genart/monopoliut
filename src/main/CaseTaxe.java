package main;
/**
 * 
 * @author lejeunes
 *
 */
public class CaseTaxe extends Case{
	
	protected int prix;

	public CaseTaxe(String nom, int prix) {
		super(nom, Type.CaseTaxe);
		this.prix = prix;
	}

	public int getPrix() {
		return prix;
	}

	
	@Override
	public void play(Joueur j) {
		if (!j.estHumain())
			System.out.println("L'IA est sur la case où il doit payer pour les humains et est débité de " + prix + "€.");
		else {
			System.out.println("Vous êtes sur la case où il faut payer la taxe pour les BDE!");
			System.out.println("Vous avez été débité de " + prix + "€.");
		}
		j.setCaisse(j.getCaisse()-prix);
		CaseParc.argent_milieu += 100;
	}
}
